import { Chat } from './src/components/Chat';
import { rootReducer } from './src/store';

export default { Chat, rootReducer };
