import { AnyMessageData } from '../types';

export const getUsersCount = (messages: AnyMessageData[]) =>
  messages.reduce(
    (userIdsList, message) => userIdsList.add('userId' in message ? message.userId : ''),
    new Set<string>()
  ).size;
