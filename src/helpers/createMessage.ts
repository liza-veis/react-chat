import { v4 as uuidv4 } from 'uuid';
import { AnyMessageData } from '../types';

export const createMessage = (data: Omit<AnyMessageData, 'id' | 'createdAt'>): AnyMessageData => ({
  ...data,
  id: uuidv4(),
  createdAt: new Date().toString(),
});
