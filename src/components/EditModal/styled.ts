import styled from 'styled-components';
import { COLOR_ACCENT, COLOR_FONT_GREY, COLOR_GREY, COLOR_WHITE } from '../../constants';

type ModalContainerProps = {
  isShown: boolean;
};

export const ModalContainer = styled.div<ModalContainerProps>`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 100;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.5);
  visibility: ${({ isShown }) => (isShown ? 'visible' : 'hidden')};
  opacity: ${({ isShown }) => (isShown ? 1 : 0)};
`;

export const Modal = styled.div`
  max-width: 500px;
  width: 100%;
  background-color: ${COLOR_WHITE};
`;

export const ModalHeader = styled.div`
  padding: 20px;
  text-align: center;
  border-bottom: 1px solid ${COLOR_GREY};
`;

export const ModalTitle = styled.h3`
  margin: 0;
  line-height: 1;
  font-size: 1.25rem;
  font-weight: bold;
  color: ${COLOR_ACCENT};
`;

export const ModalBody = styled.div`
  padding: 20px;
`;

export const ModalTextarea = styled.textarea`
  width: 100%;
  min-height: 180px;
  padding: 5px 10px;
  border: 1px solid ${COLOR_GREY};
  font-family: inherit;
  font-size: inherit;
  resize: none;
  outline: none;
`;

export const ModalFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 0 20px 20px;
  gap: 20px;
`;

type ModalButtonProps = {
  accent?: boolean;
};

export const ModalButton = styled.button<ModalButtonProps>`
  min-width: 100px;
  padding: 10px;
  background-color: ${({ accent }) => (accent ? COLOR_ACCENT : COLOR_FONT_GREY)};
  color: ${COLOR_WHITE};
  line-height: 1;
  font-family: inherit;
  font-size: inherit;
  border: none;
  outline: none;
  cursor: pointer;
`;
