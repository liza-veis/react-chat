import styled from 'styled-components';
import { COLOR_ACCENT, COLOR_GREY, COLOR_MESSAGE, COLOR_WHITE } from '../../constants';

export const MessageInput = styled.form`
  position: relative;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding: 15px 20px;
  background-color: ${COLOR_WHITE};
  box-shadow: 0 0 5px ${COLOR_GREY};
`;

export const MessageInputText = styled.input`
  width: 100%;
  padding: 10px 20px;
  font-size: inherit;
  font-family: inherit;
  background-color: ${COLOR_MESSAGE};
  border-radius: 30px;
  border: none;
  outline: none;
`;

export const MessageInputButton = styled.button`
  position: absolute;
  width: 40px;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-left: 10px;
  border-radius: 50%;
  background-color: ${COLOR_ACCENT};
  color: ${COLOR_WHITE};
  font-size: 18px;
  border: none;
  cursor: pointer;
  outline: none;
`;
