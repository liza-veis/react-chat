import { FC, Fragment, useEffect, useRef, useState } from 'react';
import { splitMessagesByDate } from '../../helpers';
import { AnyMessageData } from '../../types';
import { Message } from '../Message';
import { OwnMessage } from '../OwnMessage';
import * as S from './styled';

type MessageListProps = {
  messages: AnyMessageData[];
};

export const MessageList: FC<MessageListProps> = ({ messages }) => {
  const [messagesLength, setMessagesLength] = useState(0);

  const splitMessages = splitMessagesByDate(messages);
  const bottomElementRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (messagesLength === messages.length) return;

    bottomElementRef.current?.scrollIntoView();
    setMessagesLength(messages.length);
  }, [messages, messagesLength]);

  return (
    <S.MessageList className="message-list">
      {Object.entries(splitMessages).map(([date, messages]) => (
        <Fragment key={date}>
          <S.MessagesDivider className="messages-divider">{date}</S.MessagesDivider>
          {messages.map((message: AnyMessageData) => {
            if ('user' in message) {
              return <Message data={message} key={message.id} />;
            }
            return <OwnMessage data={message} key={message.id} />;
          })}
          <div ref={bottomElementRef} />
        </Fragment>
      ))}
    </S.MessageList>
  );
};
