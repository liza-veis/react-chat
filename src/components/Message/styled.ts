import styled from 'styled-components';
import { COLOR_ACCENT, COLOR_FONT_GREY, COLOR_MESSAGE } from '../../constants';

export const Message = styled.div`
  display: flex;
  align-items: flex-end;
  max-width: calc(50% - 100px);
  gap: 10px;
  margin-bottom: 20px;
`;

export const MessageUserAvatar = styled.img`
  width: 35px;
  height: 35px;
  flex-shrink: 0;
  border-radius: 50%;
  object-fit: cover;
  object-position: center;
`;

export const MessagesContent = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 5px 15px 10px;
  min-width: 150px;
  background-color: ${COLOR_MESSAGE};
  border-radius: 10px 10px 10px 0;
`;

export const MessageUserName = styled.span`
  font-size: 14px;
  font-weight: bold;
  color: ${COLOR_ACCENT};
`;

export const MessageText = styled.span`
  padding: 0;
`;

export const MessageAside = styled.div`
  align-self: flex-start;
  flex-shrink: 0;
`;

export const MessageTime = styled.span`
  font-size: 14px;
  color: ${COLOR_FONT_GREY};
  line-height: 1;
`;

export const ButtonLike = styled.button`
  display: flex;
  margin: 0 auto;
  font-size: 18px;
  color: ${COLOR_ACCENT};
  background-color: transparent;
  border: none;
  cursor: pointer;
  outline: none;
`;
