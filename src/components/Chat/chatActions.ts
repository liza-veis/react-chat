import { AnyMessageData } from '../../types';
import * as actionTypes from './chatActionTypes';

export const setPreloader = (value: boolean) => ({
  type: actionTypes.SET_PRELOADER,
  payload: value,
});

export const setMessages = (messages: AnyMessageData[]) => ({
  type: actionTypes.SET_MESSAGES,
  payload: messages,
});

export const addMessage = (message: AnyMessageData) => ({
  type: actionTypes.ADD_MESSAGE,
  payload: message,
});

export const setMessage = (message: AnyMessageData) => ({
  type: actionTypes.SET_MESSAGE,
  payload: message,
});

export const deleteMessage = (id: string) => ({
  type: actionTypes.DELETE_MESSAGE,
  payload: id,
});

export const setEditedMessage = (id: string) => ({
  type: actionTypes.SET_EDITED_MESSAGE,
  payload: id,
});

export const resetEditedMessage = () => ({
  type: actionTypes.RESET_EDITED_MESSAGE,
});
