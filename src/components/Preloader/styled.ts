import styled, { keyframes } from 'styled-components';
import { BiLoaderAlt } from 'react-icons/bi';
import { COLOR_ACCENT } from '../../constants';

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

export const Preloader = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const PreloaderIcon = styled(BiLoaderAlt)`
  font-size: 34px;
  color: ${COLOR_ACCENT};
  animation: ${rotate} 1.8s linear infinite;
`;
