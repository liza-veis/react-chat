export const COLOR_ACCENT = '#4a7dfa';
export const COLOR_GREY = '#cacaca';
export const COLOR_FONT_GREY = '#9c9c9c';
export const COLOR_MESSAGE = '#f6f6f6';
export const COLOR_WHITE = '#fff';
export const COLOR_BACKGROUND = '#f8f8f0';